<?php
/**
 * Created by PhpStorm.
 * User: thomasmunoz
 * Date: 27/01/15
 * Time: 22:48
 */

namespace app\controllers;

use SFramework\mvc\Controller;
use SFramework\Helpers\Input;
use SFramework\Helpers\Mail;

class HomeController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->getView()->render('home/index');
    }

    public function connaissances()
    {
        $this->getView()->render('home/connaissances');
    }

    public function contact()
    {
        $this->getView()->render('home/contact');
    }

    public function about()
    {
        $this->getView()->render('home/about');
    }
    public function send()
    {
        $input = new Input();
        $mail = $input->getPost();

        $message = '<html><b>Prénom</b> : ' . $mail['firstname'] . '<br/>';
        $message .= '<b>Nom </b>: ' . $mail['lastname'] . '<br/>';
        $message .= '<b>Mail</b> : ' . $mail['mail'] . '<br/>';
        $message .= '<b>Message</b> : ' . $mail['message'] . '</html>';

        Mail::send('contact@thomasmunoz.fr', 'Formulaire de contact - Thomasmunoz.fr', $message);

        $this->getView()->render('home/mailsuccess');
    }
}
