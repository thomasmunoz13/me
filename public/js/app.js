/**
 * Created by thomasmunoz on 28/01/15.
 */
var myApp = angular.module('MyWebsite', [])
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    });