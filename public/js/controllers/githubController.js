/**
 * Created by thomasmunoz on 29/01/15.
 */
myApp.controller('GithubController', ['$scope','REST', function($scope, REST) {

    $scope.maxUpdate = (Math.floor(Date.now() / 1000) - 3700);
    $scope.repos = [];
    REST.changeBaseUrl('https://api.github.com');

    $scope.filter = function(array){
        return array.filter(function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        });
    };

   if(localStorage.getItem('repos') === null || (localStorage.getItem('lastUpdate') != null &&
        localStorage.getItem('lastUpdate') < $scope.maxUpdate)){

        REST.REST('GET','users/thomasmunoz13/events').success(function(data){
           var repos = [];
           var tmpRepos = [];

           for(i = 0; i < data.length; ++i){
             tmpRepos[i] = data[i].repo.name;
             tmpRepos = $scope.filter(tmpRepos);
           }

           for(i = 0; i < 5; ++i){
               repos[i] = {};
               repos[i].name = tmpRepos[i].split('/')[1].charAt(0).toUpperCase() + tmpRepos[i].split('/')[1].slice(1);
               repos[i].fullname = tmpRepos[i];
           }

            localStorage.setItem('repos', JSON.stringify(repos));
            localStorage.setItem('lastUpdate', Math.floor(Date.now() / 1000));
            $scope.repos = repos;
        });
    } else {
        $scope.repos = JSON.parse(localStorage.getItem('repos'));
    }


}]);