// --- file[videos.js] ---

$(document).ready(function() {

    // Make the video embeded "responsive"
    $('.responsive-video').height($('.responsive-video').width()/2);

    $(window).resize(function() {
        $('.responsive-video').height($('.responsive-video').width()/2);
    });
});

// --- file[app.js] ---

/**
 * Created by thomasmunoz on 28/01/15.
 */
var myApp = angular.module('MyWebsite', [])
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    });

// --- file[simpleAngularRestService.js] ---

/**
 * Created by thomasmunoz on 28/01/15.
 */
myApp.service('REST', function($http, $q, $timeout) {

    RestService = {};

    // Main urls for the requests
    RestService.url = "";

    RestService.REST = function(method, part, data, headers){
        RestService.testConnect();

        // Simplify the requests (because we're all lazy) :)
        headers = (headers === 'json') ? 'application/json' : headers;
        headers = (headers !== undefined) ? headers : "application/x-www-form-urlencoded";

        // Make data looks like url parameter for urlencoded requests
        //data = (headers === "application/x-www-form-urlencoded") ? $.param(data) : data;

        // Clean parameters
        data = (data !== undefined) ? data : {} ;
        part = (part !== undefined) ? part : '';

        method = (method === 'GET' || method === 'POST' || method === 'PUT' || method === 'DELETE') ? method : 'GET';

        // Return the promise that you can access like every other promise in AngularJS (best comment ever)
        return $http({
            url: RestService.url + '/' + part,
            method: method,
            headers: {'Content-Type': headers},
            data: data
        });
    };

    /* Function you can call everytime you make a request to your API
     * It wait (timeout or 2 seconds) for a response and display a message if it's not.
     */
    RestService.testConnect = function(timeout) {
        var timeoutPromise = $timeout(function() {
            canceler.resolve(); //aborts the request when timed out
            RestService.displayMessage("The server didn't respond in time, please try again.");
        }, (timeout === undefined) ? 2000 : timeout);

        var canceler = $q.defer();

        $http.get(RestService.url).success(function(){
            $timeout.cancel(timeoutPromise);
        });
    };

    // Yeah it's a simple alert(), but you can make your own message displaying if you want ...
    RestService.displayMessage = function(message){
        alert(message);
    };

    RestService.changeBaseUrl = function(url){
        RestService.url = (url === undefined) ? RestService.url : url;
    };
    return RestService;
});

// --- file[githubController.js] ---

/**
 * Created by thomasmunoz on 29/01/15.
 */
myApp.controller('GithubController', ['$scope','REST', function($scope, REST) {

    $scope.maxUpdate = (Math.floor(Date.now() / 1000) - 3700);
    $scope.repos = [];
    REST.changeBaseUrl('https://api.github.com');
    $scope.filter = function(array){
        return array.filter(function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        });
    };

   if(localStorage.getItem('repos') === null || (localStorage.getItem('lastUpdate') != null &&
        localStorage.getItem('lastUpdate') < $scope.maxUpdate)){

        REST.REST('GET','users/thomasmunoz13/repos?type=all&sort=pushed').success(function(data){
           var repos = [];
           var tmpRepos = [];

           for(i = 0; i < data.length; ++i){
             if(data[i].id != 34563785){
                tmpRepos.push(data[i].full_name);
                tmpRepos = $scope.filter(tmpRepos);
             }
           }

           for(i = 0; i < 5; ++i){
               repos.push({});
               repos[i].name = tmpRepos[i].split('/')[1].charAt(0).toUpperCase() + tmpRepos[i].split('/')[1].slice(1);
               repos[i].fullname = tmpRepos[i];
           }

            localStorage.setItem('repos', JSON.stringify(repos));
            localStorage.setItem('lastUpdate', Math.floor(Date.now() / 1000));
            $scope.repos = repos;
        });
    } else {
        $scope.repos = JSON.parse(localStorage.getItem('repos'));
    }


}]);

