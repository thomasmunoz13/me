$(document).ready(function() {

    // Make the video embeded "responsive"
    $('.responsive-video').height($('.responsive-video').width()/2);

    $(window).resize(function() {
        $('.responsive-video').height($('.responsive-video').width()/2);
    });
});