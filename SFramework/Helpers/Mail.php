<?php
namespace SFramework\Helpers;

use SFramework\Config\ConfigFileParser;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Mail
{
    private static $config;
    private function __construct()
    {

    }

    public static function send($to, $subject, $message)
    {
        if (is_null(self::$config))
            self::$config = new ConfigFileParser("app/config/mail.json");

        $transporter = Swift_SmtpTransport::newInstance()
            ->setHost(self::$config->getEntry('host'))
            ->setPort((int)self::$config->getEntry('port'))
            ->setEncryption(self::$config->getEntry('security'))
            ->setUsername(self::$config->getEntry('username'))
            ->setPassword(self::$config->getEntry('password'));

        $mailer = Swift_Mailer::newInstance($transporter);
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setBody($message, 'text/html')
            ->addTo($to)
            ->setFrom(self::$config->getEntry('sender'));
        $mailer->send($message);
    }

    private function __clone()
    {

    }
}